# Scrape results of 2020 LT election, to find out preliminary results
# for 2nd tour

import concurrent.futures
import csv
import os

from collections import defaultdict
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup


BASE_URL = "https://rezultatai.vrk.lt/"
ELECTION_URL = "https://rezultatai.vrk.lt/statiniai/puslapiai/rinkimai/1104/1/1746/rezultatai/lt/rezultataiVienm.html"
RESULTS_FILE = 'rinkimai.csv'


def primary_scrape():
    resp = requests.get(ELECTION_URL)
    soup = BeautifulSoup(resp.text, 'html.parser')
    url_elements = soup.find_all("a")
    region_urls = []
    for el in url_elements:
        href = el.attrs['href'][8:]  # drop `?scrUrl=/` prefix
        name = el.contents[0]
        region_urls.append((name, urljoin(BASE_URL, "/statiniai/puslapiai" + href)))
    return region_urls


def scrape_region_table(name, url):
    print('scraping results from: {}'.format(name))
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')
    candidate_table = soup.find_all('table')[1]
    candidates = candidate_table.find_all('tr')

    candidate_list = []
    for i in range(2, 4):
        candidate_row = candidates[i].find_all('td')
        candidate_name = candidate_row[0].find('a').contents[0]
        try:
            candidate_party = candidate_row[1].find('a').contents[0]
        except AttributeError:  # assume there's no link
            candidate_party = candidate_row[1].contents[0].strip()
        candidate_list.append((candidate_name, candidate_party))

    return (name, candidate_list)


def result_count():
    print("Counting results")
    with open(RESULTS_FILE, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|',
                            quoting=csv.QUOTE_MINIMAL)
        all_candidates = []
        reader.__next__()  # skip header row
        for row in reader:
            all_candidates.append((row[0], [(row[1], row[2]), (row[3], row[4])]))
        results = defaultdict(int)
        for region in all_candidates:
            # first candidate party count
            results[region[1][0][1]] += 1
            # second candidate party count
            results[region[1][1][1]] += 1
        sorted_res = {k: v for k, v in sorted(results.items(), key=lambda item: item[1], reverse=True)}
    return sorted_res


def result_printer(results):
    print("RESULTS:")
    for k, v in results.items():
        print(k, v)


def main():
    if not os.path.exists(RESULTS_FILE):
        print(f"No results file found ({RESULTS_FILE})")
        region_urls = primary_scrape()

        futures = []
        executor = concurrent.futures.ThreadPoolExecutor()
        for region_url in region_urls:
            fut = executor.submit(scrape_region_table, region_url[0], region_url[1])
            futures.append(fut)

        all_candidates = []
        for fut in futures:
            all_candidates.append(fut.result())

        with open(RESULTS_FILE, '+w') as csvfile:
            fieldnames = ['region', 'first candidate name', 'first candidate party',
                          'second candidate name', 'second candidate party']
            writer = csv.writer(csvfile, fieldnames, delimiter=' ', quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)
            writer.writerow(fieldnames)
            for fut in futures:
                result = fut.result()
                all_candidates.append(result)
                writer.writerow([
                    result[0],  # region name
                    result[1][0][0], result[1][0][1],  # first candidate name and party
                    result[1][1][0], result[1][1][1],  # second candidate name and party
                ])
    else:
        print(f"Results file found ({RESULTS_FILE})")

    res = result_count()
    result_printer(res)


if __name__ == "__main__":
    main()
